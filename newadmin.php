<?php

    define( 'USERNAME', 'oididkovskiy' );
    define( 'PASSWORD', 'qwerty' );
    define( 'FIRSTNAME', 'Alexey' );
    define( 'LASTNAME', 'Didkovskiy' );
    define( 'EMAIL', 'oididkovskiy@gmail.com' );
    include_once( 'app/Mage.php' );

    Mage::app( 'admin' );
    try {

        $adminUserModel = Mage::getModel( 'admin/user' );
        $adminUserModel->setUsername( USERNAME )
            ->setFirstname( FIRSTNAME )
            ->setLastname( LASTNAME )
            ->setEmail( EMAIL )
            ->setNewPassword( PASSWORD )
            ->setPasswordConfirmation( PASSWORD )
            ->setIsActive( true )
            ->save();
        $adminUserModel->setRoleIds( array( 1 ) )

            ->setRoleUserId( $adminUserModel->getUserId() )
            ->saveRelations();
        echo 'User created.';

    } catch( Exception $e ) {
        echo $e->getMessage();
    }