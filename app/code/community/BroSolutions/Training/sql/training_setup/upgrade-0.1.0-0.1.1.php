<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$attribute_set_name = 'Course';
$group_name = 'General';

$date_attributes = array(
    'Training date' => 'training_date',
    'Available till' => 'available_till',
    'Send reminder notification on' => 'send_reminder_notification_on'
);
foreach ($date_attributes as $label => $attribute_code) {
    $installer->addAttribute('catalog_product', $attribute_code, array(
        'type'                       => 'datetime',
        'label'                      => $label,
        'input'                      => 'datetime',
        'backend'                    => 'training/entity_attribute_backend_datetime',
        'apply_to'                   => 'training',
        'required'                   => 1,
        'visible'                    => 1,
        'visible_on_front'           => 1,
        'filterable'                 => 1,
        'searchable'                 => 1,
        'comparable'                 => 1,
        'user_defined'               => 1,
        'is_configurable'            => 0,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'                       => ''
    ));

    $attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
    $attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
    $attribute_id = $installer->getAttributeId('catalog_product', $attribute_code);
    $installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
}

$fieldList = array(
    'price',
    'special_price',
    'cost',
    'tax_class_id'
);

// make these attributes applicable to downloadable products
foreach ($fieldList as $field) {
    $applyTo = split(',', $installer->getAttribute('catalog_product', $field, 'apply_to'));
    if (!in_array('training', $applyTo)) {
        $applyTo[] = 'training';
        $installer->updateAttribute('catalog_product', $field, 'apply_to', join(',', $applyTo));
    }
}

$installer->addAttribute('catalog_product', "type_training_product", array(
        'type'                       => 'int',
        'label'                      => "Type of training",
        'input'                      => 'select',
        'apply_to'                   => 'training',
        'required'                   => 1,
        'visible'                    => 1,
        'visible_on_front'           => 1,
        'filterable'                 => 1,
        'searchable'                 => 1,
        'comparable'                 => 1,
        'user_defined'               => 1,
        'is_configurable'            => 0,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'                       => '',
        'option' =>
            array (
                'values' =>
                    array (
                        0 => 'Change your option 1',
                        1 => 'Change your option 2',
                        2 => 'Change your option 3'
                    ),
            ),
    )
);

$attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
$attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
$attribute_id = $installer->getAttributeId('catalog_product', 'type_training_product');
$installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);


$add_info_attributes = array(
    'Time spending'             => 'time_spending',
    'Location'                  => 'location_training',
    'Telephone for consultation'=> 'phone_consultation',
    'Instructor'                => 'instructor'
);
foreach ($add_info_attributes as $label => $attribute_code) {
    $installer->addAttribute('catalog_product', $attribute_code, array(
        'type'                       => 'text',
        'label'                      => $label,
        'input'                      => 'text',
        'backend'                    => '',
        'apply_to'                   => 'training',
        'required'                   => 0,
        'visible'                    => 1,
        'visible_on_front'           => 1,
        'filterable'                 => 1,
        'searchable'                 => 1,
        'comparable'                 => 1,
        'user_defined'               => 1,
        'is_configurable'            => 0,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'                       => ''
    ));

    $attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
    $attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
    $attribute_id = $installer->getAttributeId('catalog_product', $attribute_code);
    $installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
}

$installer->addAttribute('catalog_product', "characteristic", array(
        'type'                       => 'text',
        'label'                      => "Characteristic",
        'input'                      => 'textarea',
        'apply_to'                   => 'training',
        'required'                   => 0,
        'visible'                    => 1,
        'visible_on_front'           => 1,
        'filterable'                 => 1,
        'searchable'                 => 1,
        'comparable'                 => 1,
        'user_defined'               => 1,
        'is_configurable'            => 0,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'                       => '',
        'wysiwyg_enabled'            => true,
        'visible_on_front'           => true,
        'is_html_allowed_on_front'   => true
    )
);

$attribute_id = $installer->getAttributeId('catalog_product', 'characteristic');
$installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);



$installer->endSetup();

//$installer = $this;
//$installer->startSetup();
//$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
//$entityTypeId = Mage::getModel('catalog/product')
//    ->getResource()
//    ->getEntityType()
//    ->getId(); //product entity type
//
//$attributeSet = Mage::getModel('eav/entity_attribute_set')
//    ->setEntityTypeId($entityTypeId)
//    ->setAttributeSetName("test_set");
//
//$attributeSet->validate();
//$attributeSet->save();
//
//$attributeSet->initFromSkeleton($entityTypeId)->save();
//$installer->endSetup();
//
//$installer->startSetup();
//
//$date_attributes = array(
//    'Training date' => 'training_date',
//    'Available till' => 'available_till',
//    'Send reminder notification on' => 'send_reminder_notification_on'
//);
//foreach ($date_attributes as $label => $attribute_code) {
//    $installer->addAttribute('catalog_product', $attribute_code, array(
//        'type'                       => 'datetime',
//        'label'                      => $label,
//        'input'                      => 'datetime',
//        'backend'                    => 'training/entity_attribute_backend_datetime',
//        'apply_to'                   => 'training',
//        'required'                   => 1,
//        'visible'                    => 1,
//        'visible_on_front'           => 1,
//        'filterable'                 => 1,
//        'searchable'                 => 1,
//        'comparable'                 => 1,
//        'user_defined'               => 1,
//        'is_configurable'            => 0,
//        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
//        'note'                       => ''
//    ));
//
//    $attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
//    $attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
//    $attribute_id = $installer->getAttributeId('catalog_product', $attribute_code);
//    $installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
//}
//
//$fieldList = array(
//    'price',
//    'special_price',
//    'cost',
//    'tax_class_id'
//);
//
//// make these attributes applicable to downloadable products
//foreach ($fieldList as $field) {
//    $applyTo = split(',', $installer->getAttribute('catalog_product', $field, 'apply_to'));
//    if (!in_array('training', $applyTo)) {
//        $applyTo[] = 'training';
//        $installer->updateAttribute('catalog_product', $field, 'apply_to', join(',', $applyTo));
//    }
//}
//
//$installer->addAttribute('catalog_product', "type_training_product", array(
//        'type'                       => 'int',
//        'label'                      => "Type of training",
//        'input'                      => 'select',
//        'apply_to'                   => 'training',
//        'required'                   => 1,
//        'visible'                    => 1,
//        'visible_on_front'           => 1,
//        'filterable'                 => 1,
//        'searchable'                 => 1,
//        'comparable'                 => 1,
//        'user_defined'               => 1,
//        'is_configurable'            => 0,
//        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
//        'note'                       => '',
//        'option' =>
//            array (
//                'values' =>
//                    array (
//                        0 => 'Change your option 1',
//                        1 => 'Change your option 2',
//                        2 => 'Change your option 3'
//                    ),
//            ),
//    )
//);
//
//$attribute_set_id = $installer->getAttributeSetId('catalog_product', $attribute_set_name);
//$attribute_group_id = $installer->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
//$attribute_id = $installer->getAttributeId('catalog_product', $attribute_code);
//$installer->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
//
//
//
//$installer->endSetup();