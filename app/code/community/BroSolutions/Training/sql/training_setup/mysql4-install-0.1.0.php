<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$attribute_set_name = 'Course';
$group_name = 'General';

//Create custom Attribute Set Group specially for training or curses
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId = Mage::getModel('catalog/product')
    ->getResource()
    ->getEntityType()
    ->getId(); //product entity type

$attributeSet = Mage::getModel('eav/entity_attribute_set')
    ->setEntityTypeId($entityTypeId)
    ->setAttributeSetName($attribute_set_name);

$attributeSet->validate();
$attributeSet->save();

$attributeSet->initFromSkeleton($entityTypeId)->save();


//Create an attribute of the category
//that defines the appearance of the category itself.
$this->addAttribute(
    'catalog_category',
    'change_view_category',
    array(
        'group'                     => 'General Information',
        'input'                     => 'select',
        'option'                    =>
                                    array (
                                        'values' =>
                                            array (
                                                0 => 'Default',
                                                1 => 'Calendar'
                                            ),
                                    ),
        'default'                   => array(0),
        'type'                      => 'int',
        'label'                     => 'Front view category',
        'required'                  => 1,
        'unique'                    => 0,
        'sort_order'                => 3,
        'user_defined'              => 1,
        'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'                   => 1,
        'visible_on_front'          => 1,
        'filterable'                => 1,
        'searchable'                => 1,
        'comparable'                => 1
    ));

$installer->endSetup();
