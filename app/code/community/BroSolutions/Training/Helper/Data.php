<?php
class BroSolutions_Training_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getSaleable($productId) {

        if(time() > strtotime(Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'available_till', Mage::app()->getStore()))) return false;

        return true;

    }

    public function getCustomDataTime($productDateStart){
        return date("d-m-Y", strtotime($productDateStart));
    }

    public function getListProductTraining()
    {
        $class = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('training_date')
            ->addAttributeToSelect('short_description')
            ->addAttributeToSelect('type_training_product')
            ->addAttributeToFilter('type_id', array('eq' => 'training'))
            ->addAttributeToFilter('training_date', array('gt' => date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()))))
            ->addUrlRewrite();

        return $class;
    }

    public function getListProductTrainingType()
    {
        $sortedResults = array();
        $collection = $this->getListProductTraining();
        foreach($collection as $product){
            if(!isset($sortedResults[$product->getAttributeText('type_training_product')])){
                $sortedResults[$product->getAttributeText('type_training_product')] = array($product);
            } else {
                $sortedResults[$product->getAttributeText('type_training_product')][] = $product;
            }
        };

        return $sortedResults;
    }


}
	 