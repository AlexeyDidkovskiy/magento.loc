<?php

class BroSolutions_Training_Model_Observer
{
    public function modifySaleable($observer){
           if ($observer->getProduct()->getTypeId() != 'training') return false;

            $saleable = $observer->getSalable();

            $productId = $saleable->getData("product")->getData("entity_id");

            $availableTill = Mage::helper('training')->getSaleable($productId);

            $saleable->setIsSalable($availableTill);

    }

    public function reminderStart() {

        $date = date("Y-m-d", time() + 86400); //current time

        $productCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('training_date')
            ->addAttributeToFilter('type_id', array('eq' => 'training'))
            ->addAttributeToFilter('send_reminder_notification_on', array('eq' => $date));


        $storeId = Mage::app()->getStore()->getId();
        foreach ($productCollection as $productItem){
            $productId = $productItem->getData("entity_id");
            $productName = $productItem->getData("name");
            $productDateStart = $productItem->getData("training_date");

            $productDateStartCustom = Mage::helper('training')->getCustomDataTime($productDateStart);

            $collection = Mage::getResourceModel('sales/order_item_collection')
                ->addAttributeToFilter('product_id', array('eq' => $productId))
                ->load();

            $order_ids = $collection->getColumnValues('order_id');



            foreach($order_ids as $order_id){
                $order = Mage::getModel('sales/order')->load($order_id);
                $customerName =  $order->getBillingAddress()->getFirstname();
                $customerLastName =  $order->getBillingAddress()->getLastname();
                $customerEmail =  $order->getCustomerEmail();
                $sender = Mage::getStoreConfig('training/order/identity');
//                $senderName = Mage::getStoreConfig('trans_email/ident_'.$sender.'/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_'.$sender.'/email');
                $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
//
//                $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                $emailTemplateVariables = array('name' => $customerName, 'lastName' => $customerLastName, 'productName' => $productName, 'nameStore' => $senderName, 'productDateStart' => $productDateStartCustom);

                $sender = Array('name' => $senderName, 'email' => $senderEmail);

                Mage::getModel('core/email_template')->sendTransactional("training_email_order_template", $sender, $customerEmail, $customerName, $emailTemplateVariables, $storeId);
            }
        }
    }
}