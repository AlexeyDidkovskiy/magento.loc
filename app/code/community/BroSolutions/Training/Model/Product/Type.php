<?php

class BroSolutions_Training_Model_Product_Type extends Mage_Catalog_Model_Product_Type_Virtual
{

    const TYPE_CLASS          = 'training';
    const XML_PATH_AUTHENTICATION = 'catalog/training/authentication';

    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode)
    {
        return parent::_prepareProduct($buyRequest, $product, $processMode);
    }
}