<?php

class BroSolutions_Training_Block_Adminhtml_Catalog_Product_Edit_Tab_Orders extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productordersGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Get the current product
     *
     * @return mixed Mage_Catalog_Model_Product
     *
     */
    protected function _getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Prepare grid collection object
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $product = $this->_getProduct();
        $productId = $product->getId();
        $collection = Mage::getResourceModel('sales/order_item_collection');

        if (isset($productId) && !empty($productId)) {
            $collection
                ->addFieldToFilter('product_id', $productId)
                ->getSelect()->joinInner(array(
                    'order' => $collection->getTable('sales/order')),
                    'order.entity_id = main_table.order_id',
                    array('increment_id', 'customer_firstname', 'customer_lastname', 'status', 'order_currency_code')
                );
        }

        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * Prepare the grid columns
     *
     * @return mixed $this|void
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn('real_order_id', array(
            'header' => Mage::helper('training')->__('Order #'),
            'width'  => '80px',
            'type'   => 'text',
            'index'  => 'increment_id',
        ));

//		if (!Mage::app()->isSingleStoreMode()) {
//			$this->addColumn('store_id', array(
//				'header'          => Mage::helper('training')->__('Purchased From (Store)'),
//				'index'           => 'store_id',
//				'type'            => 'store',
//				'store_view'      => true,
//				'display_deleted' => true,
//			));
//		}

        $this->addColumn('created_at', array(
            'header' => Mage::helper('training')->__('Purchased On'),
            'index'  => 'created_at',
            'type'   => 'datetime',
            'width'  => '170px',
        ));

        $this->addColumn('customer_firstname', array(
            'header' => Mage::helper('training')->__('Customer First Name'),
            'index'  => 'customer_firstname',
        ));

        $this->addColumn('customer_lastname', array(
            'header' => Mage::helper('training')->__('Customer Last Name'),
            'index'  => 'customer_lastname',
        ));

//		$this->addColumn('qty_ordered', array(
//			'header' => Mage::helper('training')->__('Qty Ordered'),
//			'width'  => '100px',
//			'type'   => 'number',
//			'index'  => 'qty_ordered',
//		));

//		$this->addColumn('row_total', array(
//			'header'   => Mage::helper('training')->__('Row Total'),
//			'index'    => 'row_total',
//			'type'     => 'currency',
//			'currency' => 'order_currency_code',
//		));

        $this->addColumn('status', array(
            'header'  => Mage::helper('training')->__('Status'),
            'index'   => 'status',
            'type'    => 'options',
            'width'   => '170px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('training')->__('CSV'));

        return parent::_prepareColumns();
    }

    /**
     * @return mixed|string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url')
            ? $this->_getData('grid_url')
            : $this->getUrl('*/*/productordersGrid', array('_current' => true));
    }

    /**
     * @param $item
     *
     * @return bool|string
     */
    public function getRowUrl($item)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $orderId = $item->getOrderId();
            if (isset($orderId) && !empty($orderId)) {
                return $this->getUrl('*/sales_order/view', array('order_id' => $orderId));
            } else {
                return false;
            }
        }

        return false;
    }
}