<?php

class BroSolutions_DevelopProduct_Model_Resource_Listuploadproduct extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('developproduct/listuploadproduct', 'upload_id');
    }
}