<?php

class BroSolutions_DevelopProduct_Model_Resource_Listuploadproduct_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('developproduct/listuploadproduct');
    }
}