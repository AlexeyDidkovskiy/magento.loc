<?php

class BroSolutions_DevelopProduct_Model_Product_Type extends Mage_Catalog_Model_Product_Type_Virtual
{

    const TYPE_CLASS          = 'developproduct';
    const XML_PATH_AUTHENTICATION = 'catalog/developproduct/authentication';

    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode)
    {
        return parent::_prepareProduct($buyRequest, $product, $processMode);
    }
}