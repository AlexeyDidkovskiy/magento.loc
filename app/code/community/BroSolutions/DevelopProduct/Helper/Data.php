<?php
class BroSolutions_DevelopProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function showTab($orders){
        $canShow = false;
        foreach($orders as $orderItem) {
            if($orderItem->getProductType() == "developproduct"){
                $canShow = true;
            }
        }
        return $canShow;
    }
}
	 