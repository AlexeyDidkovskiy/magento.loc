<?php


class BroSolutions_DevelopProduct_Adminhtml_Uploadproduct_UploadController extends Mage_Adminhtml_Controller_Action
{
    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            try {
                $model = Mage::getModel('developproduct/listuploadproduct');
                $model->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['title']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['description']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['form_key']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['form_redirect']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['order_id']), 'NotEmpty')) {
                    $error = true;
                }

                $fileName = '';
                if (isset($_FILES['url_product']['name']) && $_FILES['url_product']['name'] != '') {
                    try {
                        $fileName       = $_FILES['url_product']['name'];
                        $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
                        $fileNamewoe    = rtrim($fileName, $fileExt);
                        $fileName       = preg_replace('/\s+', '', $fileNamewoe) . time() . '.' . $fileExt;

                        $uploader       = new Varien_File_Uploader('url_product');
                        $uploader->setAllowedExtensions(array('doc', 'docx','pdf', 'jpg', 'png', 'zip')); //add more file types you want to allow
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::getBaseDir('media') . DS . 'developproduct';
                        if(!is_dir($path)){
                            mkdir($path, 0777, true);
                        }
                        $uploader->save($path . DS, $fileName );

                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        $error = true;
                    }


                }

                if ($error) {
                    throw new Exception();
                }

                $attachmentFilePath = Mage::getBaseDir('media'). DS . 'developproduct' . DS . $fileName;

                $model->setNameProduct($fileName);
                $model->setUrlProduct($attachmentFilePath);
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('developproduct')->__('Develop product was successfully written to the database'));
                $this->_redirectUrl($this->getRequest()->getParam(form_redirect));

                return;
            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('developproduct')->__('Develop product was not written to the database. Check the form fields'));
                $this->_redirectUrl($this->getRequest()->getParam(form_redirect));
                return;
            }

        } else {
            $this->_redirectUrl($this->getRequest()->getParam(form_redirect));
        }
    }

    public function massDeleteAction()
    {
        $tagIds = $this->getRequest()->getParam("developproduct");
        if(!is_array($tagIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select tag(s).'));
        } else {
            try {
                foreach ($tagIds as $tagId) {
                    $tag = Mage::getModel('developproduct/listuploadproduct')->load($tagId);
                    unlink(Mage::getBaseDir('media') . DS . 'developproduct' . DS . $tag->getData("name_product"));
                    $tag->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($tagIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }
}