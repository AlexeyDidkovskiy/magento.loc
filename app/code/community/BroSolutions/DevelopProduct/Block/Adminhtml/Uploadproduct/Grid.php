<?php
/**
 * Examples
 *
 * PHP Version 5
 *
 * @category  Examples
 * @package   Examples_AdminGridAndForm
 * @author    Mike Whitby <me@mikewhitby.co.uk>
 * @copyright Copyright (c) 2012 Mike Whitby (http://www.mikewhitby.co.uk)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      N/A
 */

/**
 * Thing grid
 *
 * This class gets instantiated by it's container, which is of type
 * {@link Examples_AdminGridAndForm_Block_Adminhtml_Thing}. This class is
 * responsible for creating the actual HTML tables for the grid
 *
 * @category Examples
 * @package  Examples_AdminGridAndForm
 * @author   Mike Whitby <me@mikewhitby.co.uk>
 */

class BroSolutions_DevelopProduct_Block_Adminhtml_Uploadproduct_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct(); # for grids, parent constructor should be called first

        $this->setId('uploadGrid'); # not sure where the grid id gets used
        $this->setDefaultSort('name'); # sets the default sort column
        $this->setDefaultDir('asc'); # sets the default sort direction
        $this->setSaveParametersInSession(true); # this sets filters and sorts in the session, as opposed to using the url
    }

    protected function _getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * Prepare grid collection object
     *
     * @return Examples_AdminGridAndForm_Block_Adminhtml_Thing_Grid
     */
    protected function _prepareCollection()
    {
        $order = $this->_getOrder();
        $orderId = $order->getId();
        $collection = Mage::getModel('developproduct/listuploadproduct')->getCollection();

        if (isset($orderId) && !empty($orderId)) {
            $collection
                ->addFieldToFilter('order_id', $orderId);
        }

        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;


    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'upload_id', # the column id
            array(
                'type'     => 'number', # needed for using a ranged filter
                'header'   => Mage::helper('developproduct')->__('ID'),
                'width'    => '50px',
                'index'    => 'upload_id', # index is the name of the data in the entity
                'sortable' => true, # defaults to true so this is pointless, just using as an example, can be true or false
            )
        );
        $this->addColumn(
            'title',
            array(
                'header' => Mage::helper('developproduct')->__('Title'),
                'width'  => '400px',
                'index'  => 'title',
            )
        );
        $this->addColumn(
            'description',
            array(
                'header' => Mage::helper('developproduct')->__('Description'),
                'index'  => 'description',
                'filter' => false,
            )
        );
        $this->addColumn(
            'URL Product',
            array(
                'header' => Mage::helper('developproduct')->__('URL Upload Product'),
                'index'  => 'url_product',
                'filter' => false,
            )
        );


        return parent::_prepareColumns();
    }

    public function getMainButtonsHtml()
    {
        return '';
    }

    /**
     * Return a URL to be used for each row
     *
     * If you don't wish rows to return a URL, simply omit this method
     *
     * @param Varien_Object $row The row for which to supply a URL
     *
     * @return string The row URL
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Prepare grid mass actions
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('upload_id');
        $this->getMassactionBlock()->setFormFieldName('developproduct');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'   => Mage::helper('developproduct')->__('Delete'),
                'url'     => $this->getUrl('admin_developproduct/adminhtml_uploadproduct_upload/massDelete'),
                'confirm' => Mage::helper('developproduct')->__('Are you sure?')
            )
        );
        return $this;
    }



}
