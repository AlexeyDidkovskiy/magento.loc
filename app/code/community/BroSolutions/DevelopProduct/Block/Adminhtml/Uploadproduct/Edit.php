<?php
/**
 * Examples
 *
 * PHP Version 5
 *
 * @category  Examples
 * @package   Examples_AdminGridAndForm
 * @author    Mike Whitby <me@mikewhitby.co.uk>
 * @copyright Copyright (c) 2012 Mike Whitby (http://www.mikewhitby.co.uk)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      N/A
 */

/**
 * Thing form container
 *
 * Another container, same as {@link Examples_AdminGridAndForm_Block_Adminhtml_Thing}
 * for all intents and purposes, obviously slightly different as this is a
 * container for a form rather than a grid, so buttons are different, and a
 * different class is inherited, but things are very similar
 *
 * In the same way as the grid container, this class gets instantiated by the
 * magento layout system, see the phpdoc in the grid container class to get
 * a bit more info on how this works
 *
 * @category Examples
 * @package  Examples_AdminGridAndForm
 * @author   Mike Whitby <me@mikewhitby.co.uk>
 */
class BroSolutions_DevelopProduct_Block_Adminhtml_Uploadproduct_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct(); # for form containers, parent constructor should be called first

        $this->_objectId = 'id'; # this is the param we look for in the url to load the entity
        $this->_controller = 'adminhtml_uploadproduct'; # same as the grid container
        $this->_blockGroup = 'developproduct'; # same as the grid container
        $this->_updateButton('save', 'label', Mage::helper('developproduct')->__('Save Upload Document')); # sets the text for the save button
        # adds a save and continue edit button, not needed but nice
        $this->_addButton(
            'save_and_edit_button',
            array(
                'label'     => Mage::helper('developproduct')->__('Save and Continue Edit'),
                'onclick'   => 'saveAndContinueEdit()',
                'class'     => 'save'
            ),
            100
        );
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/sales_order/index');
    }

    /**
     * Get form save URL
     *
     * @deprecated
     * @see getFormActionUrl()
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getFormActionUrl();
    }
}
