<?php
/**
 * Examples
 *
 * PHP Version 5
 *
 * @category  Examples
 * @package   Examples_AdminGridAndForm
 * @author    Mike Whitby <me@mikewhitby.co.uk>
 * @copyright Copyright (c) 2012 Mike Whitby (http://www.mikewhitby.co.uk)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      N/A
 */

/**
 * Thing form
 *
 * This class gets instantiated by it's container, which is of type
 * {@link Examples_AdminGridAndForm_Block_Adminhtml_Thing_Edit}. This class is
 * responsible for creating the actual HTML form, with all the fieldsets and
 * inputs etc, so this is the actual <form></form> and everything in it
 *
 * What might not be obvious is that this form is used for both addition and
 * editing of whatever entity type it is you are working with. You won't get to
 * see this though as this relies on the controller registering certain data
 * so this form will act as though it is adding a new entity all the time,
 * whereas in reality you would code the controller to register some data to
 * allow it to work as an 'edit', rather than a 'new' form.
 *
 * @category Examples
 * @package  Examples_AdminGridAndForm
 * @author   Mike Whitby <me@mikewhitby.co.uk>
 */
class BroSolutions_DevelopProduct_Block_Adminhtml_Uploadproduct_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        # create the form with the essential information, such as DOM ID, action
        # attribute, method and the enc type (this is needed if you have image
        # inputs in your form, and doesn't hurt to use otherwise)
        $form = new Varien_Data_Form(
            array(
                'id'      => 'edit_form',
                'action'  => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method'  => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);


        # add a fieldset, this returns a Varien_Data_Form_Element_Fieldset object
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => Mage::helper('developproduct')->__('General Information'),
            )
        );
        # now add fields on to the fieldset object, for more detailed info
        # see https://makandracards.com/magento/12737-admin-form-field-types
        $fieldset->addField(
            'name', # the input id
            'text', # the type
            array(
                'label'    => Mage::helper('developproduct')->__('Name'),
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'name',
            )
        );
        $fieldset->addField(
            'short_description',
            'textarea',
            array(
                'label' => Mage::helper('developproduct')->__('Short Description'),
                'name'  => 'short_description',
            )
        );

        $fieldset->addField(
            'path_product',
            'file',
            array(
                'label' => Mage::helper('developproduct')->__('Path Product'),
                'name'  => 'path_product',
                'class'    => 'required-entry',
                'required' => true,
            )
        );

        return parent::_prepareForm();
    }
}
