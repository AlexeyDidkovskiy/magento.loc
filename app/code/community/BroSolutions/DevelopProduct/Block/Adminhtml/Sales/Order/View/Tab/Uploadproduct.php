<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order transactions tab
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class BroSolutions_DevelopProduct_Block_Adminhtml_Sales_Order_View_Tab_Uploadproduct
    extends Mage_Adminhtml_Block_Widget_Grid_Container
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_uploadproduct'; # this is the common prefix in the second part of the grouped class name, i.e. whatever/(this_bit)
        $this->_blockGroup = 'developproduct'; # the first part of the grouped class name, i.e. (some_module)/whatever
        $this->_headerText = Mage::helper('developproduct')->__('Upload Documents'); # sets the name in the header
        $this->_addButtonLabel = Mage::helper('developproduct')->__('Add New Upload Document'); # sets the text for the add button

        parent::__construct();

        $this->_addButton('add', array(
            'label'     => $this->getAddButtonLabel(),
            'onclick'   => "uploadform('".$this->getFormSaveUrl()."', '".Mage::getSingleton('core/session')->getFormKey()."', '".$this->getCreateUrl()."', '".$this->getOrder()->getId()."')",
            'class'     => 'add',
        ));
    }

    public function getFormSaveUrl(){
        return $this->getUrl('admin_developproduct/adminhtml_uploadproduct_upload/post');
    }

    public function getCreateUrl(){
        return $this->getUrl('*/*/view', array('order_id' => $this->getRequest()->getParam("order_id"), 'key' => $this->getRequest()->getParam("key")));
    }



    /**
     * Retrieve grid url
     *
     * @return string
     */
//    public function getGridUrl()
//    {
//        return $this->getUrl('*/sales_order/transactions', array('_current' => true));
//    }

    /**
     * Retrieve grid row url
     *
     * @return string
     */
//    public function getRowUrl($item)
//    {
//        return $this->getUrl('*/sales_transactions/view', array('_current' => true, 'txn_id' => $item->getId()));
//    }

    /**
     * Retrieve tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('developproduct')->__('Upload Product');
    }

    /**
     * Retrieve tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('developproduct')->__('Upload Product');
    }

    /**
     * Check whether can show tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        $orders = $this -> getOrder()->getAllVisibleItems();
        return Mage::helper('developproduct')->showTab($orders);
    }

    /**
     * Check whether tab is hidden
     *
     * @return bool
     */
    public function isHidden(){}

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addJs('loadform.js');


        return parent::_prepareLayout();
    }
}
